const gulp = require('gulp');
const fs = require("fs");
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const wiredep = require('wiredep').stream;

const fileinclude = require('gulp-file-include');
const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const s3 = require("gulp-s3");
const sftp = require('gulp-sftp');

const clean = require('gulp-clean');
// generated on 2016-06-01 using generator-webapp 2.1.0

// gulp.task('s3publish', s3publish);

// function s3publish() {
//   var aws = JSON.parse(fs.readFileSync('./conf/aws.json'));
//   console.log(aws);
//   console.log(conf.path.dist("**"));

//   return gulp.src(conf.path.dist("**"))
//     .pipe(s3(aws));
// }

gulp.task('s3publish', () => {

  var aws = JSON.parse(fs.readFileSync('./conf/aws.json'));
  // console.log(aws);
  // console.log(conf.path.dist("**"));
  var options = { }; // headers: {'Cache-Control': 'max-age=60'} }

  return gulp.src("./dist/**")
    .pipe(s3(aws,options));

});

gulp.task('clean-scripts', function () {
  return gulp.src('dist/scripts/**/*.js', {read: false})
    .pipe(clean());
});


gulp.task('styles', () => {
  return gulp.src('app/styles/main.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'safari >= 8', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.if('app/scripts/vendor/*.js', $.babel()))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return gulp.src(files)
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint(options))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js', {
    fix: true
  })
    .pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js', {
    fix: true,
    env: {
      mocha: true
    }
  })
    .pipe(gulp.dest('test/spec/**/*.js'));
});

gulp.task('html', ['styles', 'scripts'], () => {
  // var hash = Math.random()*10000000000000000;
  var hash = '7515748835030416';
  // return gulp.src('app/**/*.html')
  return gulp.src([
      '!app/templates/**',
      'app/**/*.html'
    ])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file',
      context: {
        hashname: hash
      }
    }))
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    //.pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('html-dev', ['styles', 'scripts'], () => {
  return gulp.src('app/**/*.html')
    .pipe(fileinclude({
      //context: {"slider": true},
      prefix: '@@',
      basepath: '@file',
    }))
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    //.pipe($.if('*.js', $.uglify()))
    //.pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    //.pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
    .pipe(gulp.dest('dist'));
});



gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', () => {
  return gulp.src('app/**/*.{eot,ttf,woff,woff2}')
    //.pipe(gulp.dest('.tmp'))
    .pipe(gulp.dest('dist'));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*.*','app/**/*.json',
    '!app/*.html',
    '!app/styles/*.scss'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('extras-serve', () => {
  return gulp.src([
    'app/*.*','app/**/*.json',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('.tmp'));
});


gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', ['fileinclude', 'styles'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });

  gulp.watch([
    'app/images/**/*',
    '.tmp/fonts/**/*'
  ]).on('change', reload);
  gulp.watch('app/*.html', ['fileinclude']);
  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/scripts/**/*.js', ['scripts']);
  //gulp.watch('app/fonts/**/*', ['fonts']);
  //gulp.watch('bower.json', ['wiredep', 'fonts']);
});

gulp.task('serve:dist', () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/node_modules': 'node_modules'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('test/spec/**/*.js').on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src('app/styles/*.scss')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('build', ['clean-scripts', 'lint', 'html', 'images', 'fonts', 'extras'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('build-dev', ['html-dev', 'images', 'fonts', 'extras'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});



gulp.task('default', ['clean'], () => {
  gulp.start('build');
});

gulp.task('fileinclude', ['extras-serve'], () => {
  return gulp.src('app/**/*.html')
  .pipe(fileinclude({
    //context: {"slider": true},
    prefix: '@@',
    basepath: '@file',
  }))
  .pipe(gulp.dest('.tmp'))
  .pipe(reload({stream: true}));
});


