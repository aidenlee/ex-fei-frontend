window.app.directive('termDeposit', ['$location', 'cApp', function($location, cApp) {
  return {
  	  transclude: true,
  	type: 'A',
	replace:true,	
	priority: 900,
	templateUrl: cApp.defaultPath+'tpl/termdeposit-directive.html',

        scope: {
        	formName: '=',
            investmentMin: '=', //use =? for optionality
            investmentMax: '=',
            investmentIncrements: '=',
            ngModel: '=',
            rates: '=',
            interests: '=',
            taxRate: '=',
            visiblility: '=',
			isModal: '=',
			existingUser: '='
        },

        link: function($scope, $element, $attrs, $controller, $timeout ) {

        	
        		console.log('vv');
	        	$scope.termDeposit = $scope.ngModel; 
	        	$scope.form = $scope.formName; 
	        	$scope.status = $scope.submitStatus; 
				$scope.isModal =  $scope.isModal;
				$scope.rates = $scope.rates;

				console.log( $scope.interests )

				$scope.$watch('existingUser', function( value ) {
			    	console.log(value)
			    	if(value !== undefined){
			    		$scope.termDeposit.amount = value.amount;

          				$scope.termDeposit.selectedRate = $scope.indexTermFromUrl(value.term);
          				$scope.termDeposit.selectedInterest = $scope.interests[value.reinvest];
			    	}
			    	
			    })
			    
				// console.log($scope.rates)
	        	if($scope.visiblility == false){
	        		$scope.visible = true;
	        	}else{
	        		$scope.visible = false;
	        	}

				var termFromUrl = $location.search().term;  //get Term parameter from URL
				// console.log(termFromUrl)
				// console.log($scope.isModal)
				$scope.indexTermFromUrl = function(newVal){
					indexes = $scope.rates.map(function(obj, index) {
						if(obj.term == newVal) {
							return index;
						}
					}).filter(isFinite);
					var getIndex = $scope.rates[indexes[0]];
					return getIndex;
				}
				$scope.indexReinvest = function(newVal){
					indexes = $scope.interests.map(function(obj, index) {
						if(obj.val == newVal) {
							return index;
						}
					}).filter(isFinite);
					var getIndex = $scope.interests[indexes[0]];
					return getIndex;
				}

				if(termFromUrl !== undefined && $scope.isModal !== true ){
					$scope.termDeposit.selectedRate = $scope.indexTermFromUrl(termFromUrl);
				}
				

			$scope.calcInterest = function(interestRate, reinvest, principal, investmentMonths,taxRate) {
					//var total =finance.CI(interestRate, compoundPerPeriod, principal, investmentMonths/12 );
					var calc = new InterestRateCalc();
					var total = calc.calc(principal, new Date(), investmentMonths, interestRate, reinvest).total;

					var interestEarned = total - principal;
					var tax = interestEarned * (taxRate/100);

					var interestEarnedAfterTax = interestEarned - tax;
					var printInterestEarnedAfterTax = interestEarnedAfterTax;
					return printInterestEarnedAfterTax;
				};

				$scope.intitialFinance = function(interestRate, principal, investmentMonths ){
					finance = new Finance(); 
					var interestRate = interestRate;
					var principal = principal;
					var investmentMonths = investmentMonths;
					var taxRate = $scope.taxRate.value;


					$scope.interestReinvested = $scope.calcInterest(interestRate, true, principal, investmentMonths, taxRate);
					$scope.interestPaid = $scope.calcInterest(interestRate, false, principal, investmentMonths, taxRate);
			
					$scope.extraInterest = ($scope.interestReinvested-$scope.interestPaid);
				};
				
				$scope.finance = function(interestRate, principal, investmentMonths ){
					$scope.intitialFinance(interestRate, principal, investmentMonths )
					if ( $scope.interests && $scope.interests[1] && $scope.termDeposit.selectedInterest && $scope.termDeposit.selectedInterest.value == $scope.interests[1].value) {
						return $scope.interestPaid ;
					}
					else {
						return $scope.interestReinvested;
					}
				};
				$scope.rateBlurred = function(rate, amount){
					// console.log($scope.termDeposit.selectedRate)

					if($scope.termDeposit.selectedRate !== undefined && $scope.investmentMin !== $scope.termDeposit.selectedRate.minimum_deposit){
						// console.log( 'atom' )
						// console.log( '~$scope.termDeposit.amount='+$scope.termDeposit.amount )
						// console.log( '~$scope.investmentMin='+$scope.investmentMin )
						console.log( '~$scope.termDeposit.selectedRate.minimum_deposit='+$scope.termDeposit.selectedRate.minimum_deposit )

						if( $scope.termDeposit.amount == $scope.investmentMin || $scope.termDeposit.amount == $scope.termDeposit.selectedRate.minimum_deposit || $scope.termDeposit.amount < $scope.termDeposit.selectedRate.minimum_deposit) {
							
							$scope.termDeposit.amount = $scope.termDeposit.selectedRate.minimum_deposit;
							$scope.changeNumber = '$'+ numberWithCommas($scope.termDeposit.selectedRate.minimum_deposit);
							$scope.investmentMin = $scope.termDeposit.selectedRate.minimum_deposit;

						}else{
							$scope.investmentMin = $scope.termDeposit.selectedRate.minimum_deposit;
						}

					}
					
					if(rate !== undefined && amount !== undefined){
						$scope.intitialFinance(rate.interestRate, amount, rate.term );
					}
				};

				function numberWithCommas(x) {
				    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				}
				$scope.inputStatus = false;

				$scope.focusin = function(){
					$scope.amountAlert = '';
					$scope.changeNumber = $scope.termDeposit.amount;
					$scope.inputStatus = true;
				};
				$scope.focusOut = function(){
					if($scope.changeNumber < $scope.investmentMin){
						$scope.amountAlert = 'Minimum amount: $' + $scope.investmentMin;
					}else{
						$scope.amountAlert = '';
					}
					$scope.changeNumber = '$'+ numberWithCommas($scope.termDeposit.amount);
					$scope.inputStatus = false;

				};

		    	$scope.$watch('termDeposit.amount', function(newVal) {
		    		// console.log($scope.inputStatus);

		    		if($scope.inputStatus == false){
		    			$scope.changeNumber = '$'+ numberWithCommas(newVal);
		    		}
					if(newVal ==  $scope.investmentMin){
						// console.log('$scope.$watch(termDeposit.amount, function(newVal) {');
						$scope.changeNumber = '$' + numberWithCommas(newVal);
					}
					// if($scope.changeNumber < $scope.investmentMax){
					// 	console.log($scope.termDeposit.amount)
					// 	$scope.changeNumber = '$' + newVal;
					// 	$scope.amountAlert = '';
					// }
					if($scope.changeNumber < $scope.investmentMax){
						// console.log($scope.termDeposit.amount)
						$scope.amountAlert = '';
					}
		    	});
		    	

		    	var timer=false;
				$scope.roundByIncrement = function(number, increment) {
			   		// console.log('small number: '+$scope.changeNumber);
		        	$scope.changeNumber = Math.ceil( number / increment ) * increment ;
			    }

				$scope.enterValue = function(investmentMin, investmentIncrements){
			    
				    if($scope.changeNumber > $scope.investmentMin && $scope.changeNumber > $scope.investmentIncrements){
				    	
				    	$scope.roundByIncrement(parseFloat($scope.changeNumber), $scope.investmentIncrements);

					}else{
						$scope.changeNumber = parseFloat($scope.changeNumber);
					}

					if ($scope.changeNumber>$scope.investmentMin && $scope.changeNumber > $scope.investmentIncrements) {
						// $scope.termDeposit.amount = $scope.changeNumber;
						if($scope.changeNumber > $scope.investmentMax){

							$scope.termDeposit.amount = $scope.investmentMax;

							$scope.changeNumber = $scope.investmentMax;
							$scope.amountAlert = 'Max amount: $' + $scope.investmentMax;
						}else{
							$scope.amountAlert = '';
							$scope.termDeposit.amount = $scope.changeNumber;
						}

					}	    	
				};

			


        } // end: Link
	
  };
}]);

