window.app.directive('identificationDetails', ['localStorageService', 'cApp', function(localStorageService, cApp) {
  return {
    transclude: true,
    type: 'A',
    replace:true,
    priority: 900,
    templateUrl: cApp.defaultPath+'tpl/identification-details-directive.html',

        scope: {
            formSubmitted: '=',
            organisationFieldLabel: '=',
            fieldLabel: '=',
            showPassportNumber: '=',
            showDriverLicenseFields: '=',
            selectedId: '=',
            ngModel: '=',
            formName: '='
        },

        link: function($scope, $element, $attrs, $controller ) {
          
          $scope.form = $scope.formName; 
          $scope.selectedId = $scope.selectedId;


        var numberOfYears = (new Date()).getYear() - 10;
        var years = $.map($(Array(numberOfYears)), function (val, i) { return i + 1900; });
        var months = $.map($(Array(12)), function (val, i) { return i + 1; });
        var days = $.map($(Array(31)), function (val, i) { return i + 1; });
        
        $scope.getYears = years.sort(function(a, b){return b-a});
        $scope.getDays = days;
        $scope.getMonths = months;
          
          $scope.result = localStorageService.get('success');

          console.log($scope.selectedLabel)
        	$scope.identificationDetails = $scope.ngModel; 

          console.log($scope.selectedId)

          
        }
	
  };
}]);

