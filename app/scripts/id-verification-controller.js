
window.app.controller('idVerificationController', ['$route', '$routeParams', '$scope', '$http', '$location', 'ModalService', '$rootScope', 'localStorageService', 'cApp',
	function($route, $routeParams, $scope, $http, $location, ModalService, $rootScope, localStorageService, cApp) {

	$scope.rootUrl = '/new-url' 
	//'http://54.66.154.119/wp-json/api/v1/termdeposits/application/new';

	$scope.result = localStorageService.get('success');

	console.log($scope.result);

	$scope.identifications =  [
		{value:'driverLicense', title: 'NZ Driver Licence'},
		{value:'passport', title: 'Passport' }
	];
	$scope.selectedId = $scope.identifications[0].value;

	$scope.activeId = function(newVal){
		console.log(newVal);
		$scope.selectedId = newVal;
	}

	$scope.identificationDetails = {
		selectedId: $scope.selectedId,
		drvierLicense: {},
		passport: {}
	};


	$scope.verifyId = function(){
		$scope.verificationSubmitted = true;

		if ($scope.verifyForm.$valid) {
			$scope.verifyingToServer ();
		}
		else {

		}
	}

	$scope.verifyModal = function() {
        ModalService.showModal({
            templateUrl: cApp.defaultPath+'tpl/verification-error.html',
            controller: 'termModalController'
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
            });
        });
    };

	$scope.verifyingToServer = function(){
		if ($scope.talkingToServer) {
			return;
		}
		$scope.talkingToServer = true;

		var req = { 
		   'terms':[]
		};

		$scope.resultTxt = '';
	    $('.application-2').addClass('processing');
	    $('.process-overlay').addClass('in');

		$http({
		method: 'POST',
		url: $scope.rootUrl,
		dataType: 'json',
		data: req,
		headers: {
			'Content-Type': 'application/json; charset=utf-8',
			'Access-Control-Allow-Origin': '*',
		}}).then(function(response) {
			console.log(response);
	        // success callback
	        $('.application-2').removeClass('processing');
	        $('.process-overlay').removeClass('in');
            if (response.status == 200 ){
                var submit = response.data.message;
                $location.path('/id-verified');
                // localStorageService.set('verificationSuccess', response.config.data);
            } else {
            	$scope.talkingToServer = false;
				$scope.verifyModal(); //Open alert Modal
                $scope.errorMsg = response.data.message;
                console.log($scope.errorMsg);
                $scope.resultTxt = $scope.errorMsg;
            }
		}, function(error) {
			$scope.talkingToServer = false;
			$scope.verifyModal(); //Open alert Modal
			console.log(error);
			$('.application-2').removeClass('processing');
			$('.process-overlay').removeClass('in');
			if (error.data && error.data.message) {
				$scope.resultTxt = error.data.message;
			}
			else {
				$scope.resultTxt = error.statusText;
			}
		});
	}

}]);