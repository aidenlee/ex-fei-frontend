window.app.controller('ModalController', ['$route', '$routeParams', '$scope', '$rootScope', 'close',  'localStorageService', 'id', 'model', 'length', 'termDepositConfig',
	function($route, $routeParams, $scope, $rootScope, close, localStorageService, id, model, length, termDepositConfig) {

	$scope.termDeposit = 
			{
				amount: 6000 // default amount of investment
			};

	$scope.formVisible=false; //???

	$scope.investmentMin = termDepositConfig.investmentMin;
	$scope.investmentMax = termDepositConfig.investmentMax;
	$scope.investmentIncrements = termDepositConfig.investmentIncrements;
	$scope.rates = termDepositConfig.rates;
	$scope.interests = termDepositConfig.interests;
	$scope.taxRate = termDepositConfig.taxRate;

	$scope.index = id;

	$scope.getModel = model;
	if(model !== undefined){
		$scope.editting = true;
		$scope.termDeposit.id = $scope.index;
		$scope.termDeposit.amount = model.amount;
		$scope.termDeposit.selectedRate = model.selectedRate;
		$scope.termDeposit.selectedInterest = model.selectedInterest;
	}

	console.log($scope.termDeposit.amount)

	if($scope.arrayLength > 1){
		$scope.buttonText = 'Save';
		console.log('now it is editing');
		
	}
	else{
		console.log('adding new item');
		$scope.buttonText = 'Add';
	}

	$scope.close = function(result) {
		
		close(null, 500); // close, but give 500ms for bootstrap to animate
		// instead of data-dismiss="modal"
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	};

	$scope.save = function(result) {
		if ($scope.modalMainForm.$valid) {
			close($scope.termDeposit,500); // close, but give 500ms for bootstrap to animate
			$scope.close()
			return true;
		}		
	};
}]);

window.app.controller('termModalController', ['$route', '$routeParams', '$scope', '$http', '$rootScope', 'close',  'localStorageService', 'cApp',
	function($route, $routeParams, $scope, $http, $rootScope, close, localStorageService, cApp ) {

	$scope.rootUrl = cApp.clientLookup;
	$scope.lookupResultTxt = false;

	$scope.quickSave = function(result) {
		if ($scope.talkingToServer) {
			return;
		}
		$scope.talkingToServer = true;
		$scope.lookupResultTxt = false;


		if ($scope.investedBeforeForm.$valid) {

			var quickClientInfo = {
				lastName: $scope.LastName, 
				number: $scope.depositOrClientNumber
			}
			$scope.quickClientInfo = quickClientInfo;
			console.log( quickClientInfo );

		    $http({
			method: 'POST',
			url: $scope.rootUrl,
			dataType: 'json',
			// data: JSON.stringify(quickClientInfo),
			data: quickClientInfo,
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Access-Control-Allow-Origin': '*',
			}}).then(function(response) {

				if(response.status == 200){
					$scope.ExistingUser = true;
					$scope.talkingToServer = false;

					$('.lookup-result-text').removeClass('error');
					$('.lookup-result-text').addClass('success');
					console.log(response);

					$scope.lookupResultTxt = 'Document matching current term deposit submission, use this data to load the Who\'s This Investment For, Maturity Instructions & Bank Account sections and close the popup.';
					$scope.existingUserData = response.data;
				}
				
			}, function(error) {

				$scope.talkingToServer = false;
				console.log(error);
				$('.lookup-result-text').removeClass('success');
				$('.lookup-result-text').addClass('error');
				$scope.lookupResultTxt = error.data.message;
				
			});

		}else{
			$scope.talkingToServer = false;
		}	
	};

	$scope.continueLookupResult = function(result) {
		// $scope.existingUserData.existingUser = new Array;
		$scope.existingUserData.push($scope.quickClientInfo);
		$scope.save($scope.existingUserData);
	};


	$scope.closeModal = function(result) {
		close(null, 500); // close, but give 500ms for bootstrap to animate
		// instead of data-dismiss="modal"
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	};

	$scope.save = function(result) {
		if ($scope.investedBeforeForm.$valid) {
			close($scope.existingUserData,500); // close, but give 500ms for bootstrap to animate
			$scope.closeModal();
			return true;
		}		
	};

}]);