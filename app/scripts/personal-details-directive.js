window.app.directive('personalDetails', ['cApp', function(cApp) {
  return {
    transclude: true,
    type: 'A',
    replace:true,
    priority: 900,
    templateUrl: cApp.defaultPath+'tpl/personal-details-directive.html',

        scope: {
           formSubmitted: '=',
            showExtraPerson: '=',
            organisationFieldLabel: '=',
            showOrganisationName: '=',
            selectedLabel: '=',
            ngModel: '=',
            formName: '='
        },

        link: function($scope, $element, $attrs, $controller ) {
          
          $scope.form = $scope.formName; 

          $scope.selectedLabel = $scope.selectedLabel;
          console.log($scope.selectedLabel)
        	$scope.personalDetails = $scope.ngModel; 

          if($scope.selectedLabel == 'Trust'){
            $scope.personalDetails.structure = $scope.personalDetails.structure;
          }else{
            $scope.personalDetails.structure = $scope.personalDetails.structure2;
          }

        $scope.result3 = '';
        $scope.options3 = {
          country: 'nz',
          types: 'geocode'
        };
        $scope.details3 = '';

          $scope.titles = ['Mr', 'Mrs', 'Miss', 'Other'];
        }
	
  };
}]);

