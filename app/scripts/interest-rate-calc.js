var InterestRateCalc = function() {};
;(function (window, document, undefined) {
	
	function daydiff(first, second) {
    	return Math.round((second-first)/(1000*60*60*24));
	}

	function isPaymentDate(date) {
		for(var i=0; i < paymentDates.length; i++) {
			var p = paymentDates[i];
			if (p.date == date.getDate() && p.month == date.getMonth()+1) {
				return true;
			}
		}
		return false;
	}

	function round(num) {
		return Math.round(num * 100) / 100;
	}

	//of Dec / march / june and september 
	var paymentDates = [
	  { date: 31, month: 3 },
	  { date: 30, month: 6 },
	  { date: 30, month: 9 },
	  { date: 31, month: 12 }
	];


	InterestRateCalc.prototype.calc = function(principal, establishmentDate, termMonths, interestRate, reinvest) {
		//console.log ("calc " + principal + ", " + establishmentDate + ", term:" + termMonths + ", " + interestRate + " , " + reinvest + " )");
		principal = parseFloat(principal);
		termMonths = parseInt(termMonths);
		interestRate = parseFloat(interestRate);

		interestRate = interestRate / 100;
		var dailyInterest = interestRate / 365;

		// Will you pay exactly X days from
		// establishment ?? or now payments until the quarter?
		//console.log ( "How many months: " + establishmentDate.getMonth()+termMonths );
		//console.log ( " months: " + establishmentDate.getMonth() );

		var endOfTerm = new Date(new Date(establishmentDate).setMonth(establishmentDate.getMonth()+termMonths));
		//console.log (endOfTerm);
		var totalDays = daydiff(establishmentDate,endOfTerm);
	
		//console.log ( totalDays );

		var today = new Date(establishmentDate);

		var totalInterestAndPrincipal = principal;
		var interestForPeriod=0;
		var totalInterestEarned=0;
		var debug=false;

		if (debug) {
			document.write ( "totalDays: " + totalDays + "<br />");
		}

		for(var dayIdx=0; dayIdx < totalDays; dayIdx++) {
			

			var interestToday = totalInterestAndPrincipal * dailyInterest;

			interestForPeriod+= interestToday;
			var timeToPay = isPaymentDate(today);

			if (timeToPay) {
				totalInterestEarned += round(interestForPeriod);	
				if (reinvest) {					
					totalInterestAndPrincipal += round(interestForPeriod);	
				}
				interestForPeriod = 0;
			}

			if (debug) {
				document.write ( "Day: " + today.getDate() + "/" + (today.getMonth()+1) +  ", interest: " + interestToday + "<br />" );
				if (timeToPay) {
					document.write ( "Earnings so far: " + totalInterestAndPrincipal + "<br />" );
				}
			}

			// Add a day
			today.setDate(today.getDate() + 1);

		}
		// ADd any remaining interest	
		totalInterestEarned += round(interestForPeriod);	
		if (reinvest) {	
			totalInterestAndPrincipal += round(interestForPeriod);	
		}
		else {
			totalInterestAndPrincipal += totalInterestEarned
		}
		
		if (debug) {
			document.write ( "Earnings so far: " + totalInterestAndPrincipal + "<br />" );
		}
		// $10,647.46 
		//var interestEarned = totalInterestAndPrincipal - principal;
		//console.log("totalInterestAndPrincipal : " + totalInterestAndPrincipal + ",   totalInterestEarned: " +  totalInterestEarned);
		return {
			interest: round(totalInterestEarned),
			total: round(totalInterestAndPrincipal)
		};
	};

})( window, document );


