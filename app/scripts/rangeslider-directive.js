window.app.directive('rangeSlider', ['cApp', function(cApp) {
  return {
	transclude:true,
	priority: 800,
	require: '?ngModel',
 	scope: {
            model: '=ngModel'
        },
    link: function($scope, $element, $attrs, $controller ){
    	// console.log('hello ' + $attrs.max);
    	
    	var min = parseInt($attrs.min);
    	var step = parseInt($attrs.step);
		$scope.$watch('model', function() {
           // console.log('Changed mmmmm'); 
           //$controller.$setViewValue($scope.model);
           if ($scope.model>min && $scope.model % step == 0) {
           		$($element).change();
   			}
        });

    	var setupSlider = function() {
    		$($element).attr('min',min );
    		$($element).attr('max',$attrs.max );
			$($element).rangeslider({ 
				polyfill: false,
				max:  $attrs.max,
				 onSlide: function(position, value) {
				 	//console.log(position);
				 	// console.log(value);
				 }
			});
			// console.log($scope.model)
			if($scope.model == undefined){
				$($element).val(min).change();
			}
			// 
		};

		setupSlider();

/*
	var rangeSlider = function(getCount){
		$(function() {
			var selector = '[data-rangeslider]';
		    var $inputRange = $(selector);
		    var $getCount = getCount;
		    $inputRange.each(function(){
		    	$(this).rangeslider({ polyfill: false });
		    })
			$(selector).val($getCount).change();

		});
	};
*/

    	/*
    	var $input = $element.find('input[data-rangeslider]');
    	var $output = $element.siblings('.change-number');

    	$scope.search = Search;

		$scope.rangeSlider = function(getCount){
			$(function() {
			    var $getCount = getCount;
		    	$input.load( 'views/another-term-deposit.html', function() {
			    	$input.rangeslider({ polyfill: false });
					$input.val($getCount).change();
				});

			});
		};
		$scope.rangeSlider($scope.search.range);
		$scope.$on('changeText',function(event, data){
             $scope.rangeSlider(data);
             console.log('broadcast');
         });
*/
    }
  };
}]);

