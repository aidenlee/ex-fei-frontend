'use strict';
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ngAutocomplete', 'angularModalService', 'LocalStorageModule']);
window.app = app;

window.app.constant('cApp', {
     'defaultPath': '/application/',
    // 'defaultPath': '/wp-content/themes/fei.co.nz/dist/application/',
    'clientLookup': 'https://dev.fei.co.nz/wp-json/api/v1/termdeposits/client-lookup',
    'applicationApi': 'https://dev.fei.co.nz/wp-json/api/v1/termdeposits/application/new',
    'InterestRatesUrl': 'https://api.fei.co.nz/wp-json/api/v1/termdeposits/interestrates'
})

app.config(['$routeProvider', '$locationProvider', 'cApp',
    function($routeProvider, $locationProvider, cApp) {

      $routeProvider
        .when('/', {
          templateUrl: cApp.defaultPath+'views/application.html',
          controller: 'ConfigApplicationCtrl'
        })
        .when('/submitted', {
          templateUrl: cApp.defaultPath+'views/submitted.html',
          controller: 'ConfirmationController'
        })
        .when('/id-verification', {
          templateUrl: cApp.defaultPath+'views/id-verification.html',
          controller: 'idVerificationController'
        })
        .when('/id-verified',{
          templateUrl: cApp.defaultPath+'views/id-verified.html',
          controller: 'ConfirmationController'
        })
        .otherwise({
            redirectTo: '/'
        });


       // use the HTML5 History API
        $locationProvider.hashPrefix('');
        $locationProvider.html5Mode(false);

  }]);  