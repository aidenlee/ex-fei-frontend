
window.app.controller('ConfigApplicationCtrl', ['$route', '$routeParams', '$location', '$http', '$scope', 'ModalService', '$rootScope', 'localStorageService', '$timeout', 'cApp',	function($route, $routeParams, $location, $http, $scope, ModalService, $rootScope, localStorageService, $timeout, cApp) {

	// $scope.rootUrl = 'https://api.fei.co.nz/wp-json/api/v1/termdeposits/application/new'; //Production
	$scope.rootUrl = cApp.applicationApi; // Dev
	
	$scope.InterestRatesUrl = cApp.InterestRatesUrl;

	// Default values for form
 	$scope.investmentMin = 5000; 
	$scope.investmentIncrements = 500; 
	$scope.investmentMax = 1000000; 


    $http({
        method : 'GET',
        url : $scope.InterestRatesUrl
    }).then(function mySucces(response) {
    	console.log(response)
        $scope.rates = response.data;
        return false;
    }, function myError(response) {
    	// console.log(response)
        $scope.myWelcome = response.statusText;
    });

	// Rates they can pick from 

	$scope.interests = [
		{
			title: 'Reinvest Interest',
			value: true
		},
		{
			title: 'Pay Interest',
			value: false
		}
	]

	$scope.maturityInstructions = [
		{
			title: 'Reinvest all funds', 
			description: 'Maximise my returns by reinvesting my orginal investment plus any interest outstanding for the same term as the initial deposit',
			value: 'reinvestall'
		},
		{
			title: 'Reinvest the principal, pay me the interest', 
			description: 'Pay me the outstanding interest (if any) and reinvest the principal for the same term as the initial deposit',
			value: 'reinvestprincipal'
		},
		{
			title: 'Repay all funds to me', 
			description: 'Return my original investment plus all interest outstanding to me.',
			value: 'payall'
		},
	];

	$scope.NoInvestAnotherTerm = true;
	$scope.rWTrate = [
		{name:'33%', value: 33},
		{name:'10.5%', value: 10.5},
		{name:'17.5%', value: 17.5},
		{name:'28%', value: 28},
		{name:'30%', value: 30},		
		{name:'2% (AIL)', value: 2},
		{name:'0% (RWT Exempt)', value: 0}
	];

	$scope.taxRate = $scope.rWTrate[0];

	// $scope.taxupdate = function(){
	// 	$scope.selectedTaxRate = $scope.getRWTrate.value;
	// };

	$scope.identifications = [
		{title: 'Perform online identification using by driver license or passport'},
		{title: 'I’ll post in a certified copy of my driver license or passport'}
	];


	$scope.owners = [
		{value:'Individual', title: 'Me'},
		{value:'Joint', title: 'Me and another person' },
		{value:'Trust', title: 'Trust'},
		{value:'Company', title: 'Company'}
	];

	$scope.selectedOwner = $scope.owners[0].value;
	$scope.visiblility = true;
	$scope.applicationForm = {
		personalDetails: {},
		termDeposits: [
			{
				amount: 5000
			}
		]
	};

	$scope.previousDepositNumber = '';
	$scope.previousInvestorNumber = '';

	$scope.editModal = function(index, item, length) {
    	$scope.modal(index, item, length);
	};
	$scope.deleteTermDeposit = function(index) {
		// console.log(index);
		console.log($scope.applicationForm.termDeposits);

		$scope.applicationForm.termDeposits.splice(index, 1);
	}
	// Used to add additional term deposit amounts
	$scope.addAdditionalTermDeposit = function(){
		$scope.modal();
	};

    $scope.modal = function(index, item, length) {
        ModalService.showModal({
            templateUrl: cApp.defaultPath+'views/another-term-deposit.html',
            controller: 'ModalController',
            inputs: {
            	termDepositConfig: {
            		investmentMin: $scope.investmentMin,
            		investmentMax: $scope.investmentMax,
            		investmentIncrements: $scope.investmentIncrements,
            		rates: $scope.rates,
            		interests: $scope.interests,
            		taxRate: $scope.taxRate
            	},
              id: index,
              model: item,
              length: length
            }
        }).then(function(modal) {
            modal.element.modal();

            modal.close.then(function(result) {
            	if (result!=null) { // They changed or added a term deposit
            		console.log (result.id);
            		if(result.id !== undefined){
						$scope.applicationForm.termDeposits[result.id].amount = result.amount;
						$scope.applicationForm.termDeposits[result.id].selectedRate = result.selectedRate;
						$scope.applicationForm.termDeposits[result.id].selectedInterst = result.selectedInterst;
            		}else{
            			$scope.applicationForm.termDeposits.push(result);
            		}
					
            	}
            });
        });
    };
    $scope.termsmodal = function(index, item) {
        ModalService.showModal({
            templateUrl: cApp.defaultPath+'/tpl/terms-and-conditions.html',
            controller: 'termModalController',
            inputs: {
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
            	if (result!=null) { // They changed or added a term deposit
            		if(result.id !== undefined){
            			
            		}else{
            		}
            	}
            });
        });
    };


	$scope.investedBefore = [
		{value:true, title: 'Yes'},
		{value:false, title: 'No' }
	];

  
    $scope.investedBeforemodal = function(index, item) {
        ModalService.showModal({
            templateUrl: cApp.defaultPath+'views/invested-before.html',
            controller: 'termModalController'
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
            	console.log (result);

            	if (result !== null) { // They changed or added a term deposit
            		
            		var existingUser = result[0];

            		if(existingUser.origin.get_field == 'client_number'){
            			$scope.previousInvestorNumber = result[1].number;
            		}else if(existingUser.origin.get_field == 'deposit_number'){
            			$scope.previousDepositNumber = result[1].number;
            		}

            		console.log($scope.previousDepositNumber)
            		
					function getIndexRWTrateOf(arr, val, filterModel) {
					    for (var k = 0; k < arr.length; k ++) {
					      	if(filterModel == 'rWTrate'){
					      		if ( arr[k].name == val ) {
						          var getIndex =  k;
						        }
					      	}else if(filterModel == 'maturityInstructions' || filterModel == 'entity_type' || filterModel == 'reinvest'){
					      		if ( arr[k].value == val ) {
						          var getIndex =  k;
						        }
					      	}
					      		// $scope.owners.value	= 'entity_type'
					    }
					    return arr[getIndex];
				    }

					terms = [];
            		for(var i = 0; i < existingUser.terms.length; i++){
			 			var itemToCopy = existingUser.terms[i];
			 			//console.log('ITEM TO COPY: ' + itemIdx);
			 			//console.log (JSON.stringify(itemToCopy));
			 			var termAnd = { 
					        amount: parseInt(itemToCopy.amount), 
					        term: itemToCopy.term, 
					        interest: itemToCopy.interest, 
					        reinvest: itemToCopy.reinvest,
					        selectedRate:{
					        	term: itemToCopy.term,
				        		interestRate: itemToCopy.interest
					        },
					        selectedInterest: getIndexRWTrateOf( $scope.interests, itemToCopy.reinvest, 'reinvest' ),
					        length: i

				    	};
				    	terms.push(termAnd);
				    	i++;
				    }
				    console.log($scope.applicationForm.termDeposits)
				    console.log($scope.applicationForm.termDeposits.length )

				    if($scope.applicationForm.termDeposits.length <= 1){
				    	$scope.existData = termAnd;
				    	$scope.applicationForm.termDeposits.splice(0, existingUser.terms.length);
				    }
				    $scope.applicationForm.termDeposits.push(termAnd);

				    // if(  $scope.applicationForm.termDeposits.length > 1){
				    // 	console.log('88')
        //   				$scope.applicationForm.termDeposits.push(termAnd);
        //   			}else{
        //   				$scope.applicationForm.termDeposits.splice(0, existingUser.terms.length);
        //   				$scope.existData = termAnd; //Put value in Directive

				    //     $scope.applicationForm.termDeposits.push(termAnd);
				    //     console.log($scope.applicationForm.termDeposits)
				    //     console.log('99')
        //   			}

      				console.log($scope.applicationForm.termDeposits)

					$scope.irdNumber1 = existingUser.ird_number; //IRD Number (01)*
					$scope.irdNumber2 = existingUser.ird_number2; //IRD Number (02)

					$scope.taxRate = getIndexRWTrateOf( $scope.rWTrate, existingUser.tax, 'rWTrate' ); //Please deduct RWT at the rate of
				    $scope.selectedMaturityInstructions = getIndexRWTrateOf( $scope.maturityInstructions, existingUser.instruction_type, 'maturityInstructions' );

					$scope.bankName = existingUser.bank;
					$scope.accountNumber = existingUser.account_number;
					$scope.bankAccountName = existingUser.bank_account_name;
					$scope.selectedOwner = getIndexRWTrateOf( $scope.owners, existingUser.entity_type, 'entity_type' );
					$scope.activeTab($scope.selectedOwner.value); // Change Selected Tab for "Who's This Investment For"


					$scope.applicationForm.personalDetails.email =  existingUser.email;
					$scope.applicationForm.personalDetails.title = existingUser.contact_title;
					$scope.applicationForm.personalDetails.surname = existingUser.surname;
					$scope.applicationForm.personalDetails.firstName = existingUser.firstname;

					$scope.applicationForm.personalDetails.title2 = existingUser.title2;
					$scope.applicationForm.personalDetails.surname2 = existingUser.surname2;
					$scope.applicationForm.personalDetails.firstName2 = existingUser.firstName2;

					$scope.applicationForm.personalDetails.mobilePhone = existingUser.mobile;
					$scope.applicationForm.personalDetails.contactPhone = existingUser.contact;
					$scope.applicationForm.personalDetails.address = existingUser.postal_address;


            	}else{
					$scope.selectedInvestedBefore = null;
            	}
            });
        });
    };

	$scope.$watch('selectedInvestedBefore', function( value ) {
    	console.log(value)
    	// $scope.investedBeforemodal();
    })

	$scope.investedBeforeInfo = function(newVal){
		console.log(newVal)
		if( newVal == true){
			$scope.investedBeforemodal();
		}
	};

	$scope.$watch('selectedMaturityInstructions', function( value ) {
    	// console.log($scope.applicationForm)
    })

    // $scope.$watch('selectedId', function( value ) {
    // 	console.log(value)
    // })
    
	$scope.IrdNumber2 = false;

	$scope.activeTab = function(newVal){
		console.log(newVal);
		$scope.selectedOwner = newVal;

		if(newVal == 'Joint'){
			$scope.IrdNumber2 = true;
		}else{
			$scope.IrdNumber2 = false;
		}
	}

	if($scope.irdNumber2 == undefined){
		$scope.irdNumber2 = '';
	}
	if($scope.applicationForm.personalDetails.structure == undefined){
		$scope.applicationForm.personalDetails.structure = '';
	}


	function getEntityName (){
		console.log($scope.selectedOwner);
		if($scope.selectedOwner == 'Individual'){
			$scope.entityName = $scope.applicationForm.personalDetails.title +'. '+ $scope.applicationForm.personalDetails.firstName +' '+$scope.applicationForm.personalDetails.surname;
		}
		if($scope.selectedOwner == 'Joint'){
			$scope.entityName = $scope.applicationForm.personalDetails.title +'. '+ $scope.applicationForm.personalDetails.firstName +' '+$scope.applicationForm.personalDetails.surname
			+ '/ ' + $scope.applicationForm.personalDetails.title2 +'. '+ $scope.applicationForm.personalDetails.firstName2 +' '+$scope.applicationForm.personalDetails.surname2;
		}
		if($scope.selectedOwner == 'Trust'){
			$scope.entityName = $scope.applicationForm.personalDetails.structure;
		}
		if($scope.selectedOwner == 'Company'){
			$scope.entityName = $scope.applicationForm.personalDetails.structure1;
		}
	}
	// && IrdNumber2 = false
	// var nodes = [];
 //    $scope.node = {};

	$scope.submitApplication = function(){

		$scope.appSubmitted = true;
		getEntityName();

		//console.log( JSON.stringify($scope.applicationForm)) ;

		if ($scope.mainForm.$valid) {
			$scope.submitAppToServer (); 
		}
		else {
			// Not valid scroll to the error
			$('.ng-invalid').last().addClass( 'highlight' );
				$timeout(function() {
					$('html, body').animate({
	        			scrollTop: $('.ng-invalid').not('form').offset().top-20
	    			}, 500);
				}, 100);
		}
	}

	$scope.uncheck = function (event) {
	    if ($scope.identification == event.target.value)
	        $scope.checked = false
	}

	var config = {};

	$scope.submitAppToServer = function() {
		
		if ($scope.talkingToServer) {
			return;
		}
		$scope.talkingToServer = true;
		// console.log($scope.applicationForm)
		// console.log($scope.applicationForm.termDeposits)
		// console.log($scope.applicationForm.termDeposits.selectedInterest)
		

		var req = { 
			'previousDepositNumber': $scope.previousDepositNumber,
			'previousInvestorNumber': $scope.previousInvestorNumber,
		   'terms':[], 
		   'owner':{ 
		      'type': $scope.selectedOwner.toLowerCase(),  
		      'entity':{ 
		         'name': $scope.entityName
		      } 
		   }, 
		   'contactDetails':{ 
		      'email': $scope.applicationForm.personalDetails.email, 
		      'title':$scope.applicationForm.personalDetails.title, 
		      'surname': $scope.applicationForm.personalDetails.surname, 
		      'firstName': $scope.applicationForm.personalDetails.firstName, 

		      'title2':$scope.applicationForm.personalDetails.title2, 
		      'surname2': $scope.applicationForm.personalDetails.surname2, 
		      'firstName2': $scope.applicationForm.personalDetails.firstName2, 

		      'phones':{ 
		         'mobile': $scope.applicationForm.personalDetails.mobilePhone, 
		         'contact': $scope.applicationForm.personalDetails.contactPhone
		      }, 
		      'postalAddress':{ 
		         'fullAddress':$scope.applicationForm.personalDetails.address
		      } 
		   }, 
		   'tax':{ 
		      'irdNumber':$scope.irdNumber1, 
		      'irdNumber2':$scope.irdNumber2, 
		      'taxRate': $scope.taxRate.value
		   }, 
		   'maturityInstructions':{ 
		      'action': $scope.selectedMaturityInstructions.value, 
		      'amount': '',
		      'bank': $scope.bankName,
		      'bankAccountName': $scope.bankAccountName,
		      'accountNumber': $scope.accountNumber
		   } 
		};
		console.log($scope.applicationForm)
		console.log($scope.applicationForm.termDeposits.length)

		for( var i =0; i < $scope.applicationForm.termDeposits.length; i++){
			var itemToCopy = $scope.applicationForm.termDeposits[i];
			
			console.log(i)

			var termAnd = { 
	        'amount': parseInt(itemToCopy.amount), 
	        'term': itemToCopy.selectedRate.term, 
	        'interest':itemToCopy.selectedRate.interestRate, 
	        'reinvest':itemToCopy.selectedInterest.value
	    	};
	    	req.terms.push(termAnd);
	    }
	    
		console.log(req);
	    
	    $scope.resultTxt = '';
	    $('.application-2').addClass('processing');
	    $('.process-overlay').addClass('in');
		
		$http({
		method: 'POST',
		url: $scope.rootUrl,
		dataType: 'json',
		data: req,
		headers: {
			'Content-Type': 'application/json; charset=utf-8',
			'Access-Control-Allow-Origin': '*',
		}}).then(function(response) {
			console.log(response);
			
	        // success callback
	        $('.application-2').removeClass('processing');
	        $('.process-overlay').removeClass('in');
            if (response.status == 200 ){
                var submit = response.data.message;

                var appRes = response.data.application;
                var totalInvestment=0;
                for(var termIdx in appRes.body.terms) {
                	totalInvestment += parseFloat(appRes.body.terms[termIdx].amount);
                }
                localStorageService.set('success', response.config.data);
                localStorageService.set('application', response.data.application);                

                if (window['google_tag_manager']) { // GTM is installed on the page
	                // Track the event to GTM
	                window.dataLayer = window.dataLayer || [];
	                var applicationNo = appRes['application number'];

	                // Pass transaction to GTM
	                // for GA
					var gaTrans = {
					   'transactionId': applicationNo,
					   'transactionTotal': totalInvestment,
					   'transactionTax': 0,
					   'transactionShipping': 0,
					   'transactionProducts': []
					};
	                for(var termIdx in appRes.body.terms) {
	                	var td = appRes.body.terms[termIdx];
                		gaTrans.transactionProducts.push (
						{
					       'sku': td.term + td.interest ,
					       'name': 'Term Deposit (' + td.term + ' months @ ' + td.interest + '%' ,
					       'category': 'Term deposit ' + td.term,
					       'price': parseFloat(td.amount),
					       'quantity': 1
					   });
                	}
					window.dataLayer.push( gaTrans );
					// This triggers the actual logging to GTM
					window.dataLayer.push(
						{
							'event' : 'Application Submitted',
							'TotalInvestment': totalInvestment, 
							'ApplicationNumber': applicationNo,
							'eventCallback' : function() {
	        					// Show the results page once the event has been sent to GTM
	        					console.log('Event Callback Complete');
	                			//window.open('/application/#/submitted', "_self");
	                			// window.location.href = '/application/#/submitted';
	                			window.location.href = '//'+window.location.host+'/application/#/submitted';
								// window.location.reload();
	    					},
	    					'eventTimeout' : 2000 // Wait a max of 2 sec for the GTM to fire
						}
					);
				}
				else { // No GTM
 					// window.location.href = '/application/#/submitted';
 					window.location.href = '//'+window.location.host+'/application/#/submitted';
					// window.location.reload();
				}

            } else {
            	$scope.talkingToServer = false;
                $scope.errorMsg = response.data.message;
                console.log($scope.errorMsg);
                $scope.resultTxt = $scope.errorMsg;
            }
		}, function(error) {
			$scope.talkingToServer = false;
			console.log(error);
			$('.application-2').removeClass('processing');
			$('.process-overlay').removeClass('in');
			if (error.data && error.data.message) {
				$scope.resultTxt = error.data.message;
			}
			else {
				$scope.resultTxt = error.statusText;
			}
		});


	}; //$scope.submitAppToServer

}]);
