
window.app.controller('ConfirmationController', ['$route', '$routeParams', '$scope', '$rootScope', 'localStorageService',
	function($route, $routeParams, $scope, $rootScope, localStorageService) {

	$scope.result = localStorageService.get('success');
	var returnapp = localStorageService.get('application');
	$scope.appNumber = returnapp['application number'];
	console.log($scope.appNumber);

	$scope.fullName = $scope.result.contactDetails.firstName +' '+ $scope.result.contactDetails.surname;

	$scope.startUp = function(){
		$(document).ready(function(){
			$('html, body').animate({
			scrollTop: 0
			}, 400);
    	});
	}
	$scope.startUp();

	$scope.calcInterest = function(interestRate, compoundPerPeriod, principal, investmentMonths,taxRate) {
		var total =finance.CI(interestRate, compoundPerPeriod, principal, investmentMonths/12 );

		var interestEarned = total - principal;
		var tax = interestEarned * (taxRate/100);

		var interestEarnedAfterTax = interestEarned - tax;
		var printInterestEarnedAfterTax = interestEarnedAfterTax;
		return printInterestEarnedAfterTax;
	};

	$scope.finance = function(interestRate, principal, investmentMonths ){
		finance = new Finance(); 
		var interestRate = interestRate;
		var principal = principal;
		var investmentMonths = investmentMonths;
		var taxRate = $scope.result.tax.taxRate;

		var interestReinvested = $scope.calcInterest(interestRate, 4, principal, investmentMonths, taxRate);

		return interestReinvested;
	};

	$scope.calcInterestYouwillGet = function(interest, amount, term){
		return $scope.finance(interest, amount, term);
	}


	$scope.getMaturityInstructions = function(newVal){
		if(newVal == 'reinvestall'){
			$scope.ListOfMaturityInstructions = {
				title:'Reinvestment of principal and repayment of all interest outstanding(if any).',
				description: 'Maximise my returns by reinvesting my orginal investment plus any interest outstanding for the same term as the initial deposit'
			}
		}
		if(newVal == 'reinvestprincipal'){
			$scope.ListOfMaturityInstructions = {
				title: 'Reinvest the principal, pay me the interest', 
				description: 'Pay me the outstanding interest (if any) and reinvest the principal for the same term as the initial deposit'
			}
		}
		if(newVal == 'payall'){
			$scope.ListOfMaturityInstructions = {
				title: 'Repay all funds to me', 
				description: 'Return my original investment plus all interest outstanding to me.'
			}
		}
	}

	console.log( $scope.result )

	$scope.getMaturityInstructions($scope.result.maturityInstructions.action);

	$scope.getTotalInvestAmounts = function(newVal){
		if(newVal.length > 1){
			$scope.answerValue = 0; 
		    for(i=0; i < newVal.length; i++) { 
		        $scope.answerValue += Number(newVal[i].amount);
		    } 
		    return $scope.answerValue;
		    
		}else{
			return newVal[0].amount;
		}
	}

}]);
